# Prompt the user to enter the path to the directory containing the images
$sourceDir = Read-Host "Enter the path to the directory containing your images"
$exifToolPath = "C:\ExifTool\exiftool(-k).exe" # Path to ExifTool executable

# Verify that the directory exists
if (-not (Test-Path -Path $sourceDir)) {
    Write-Host "The specified directory does not exist. Please try again." -ForegroundColor Red
    exit
}

# Get all image files in the source directory (including subdirectories)
$imageFiles = Get-ChildItem -Path $sourceDir -Include *.jpg, *.jpeg, *.png, *.gif, *.bmp, *.HEIC -File -Recurse

foreach ($file in $imageFiles) {
    # Get the DateTaken from the image metadata using ExifTool
    $exifData = & $exifToolPath -DateTimeOriginal -s3 -d "%Y-%m-%d_%H-%M-%S" $file.FullName

    if ($exifData) {
        $dateTaken = $exifData.Trim()
        
        # Format the new name based on the DateTaken
        $newNameBase = $dateTaken
        $newName = $newNameBase + $file.Extension

        # Define the full path for the new name
        $newFullName = Join-Path -Path $file.DirectoryName -ChildPath $newName

        # Initialize a counter to handle naming conflicts
        $counter = 1

        # If a file with the new name already exists, modify the name to include a counter
        while (Test-Path -Path $newFullName) {
            $newName = $newNameBase + "_$counter" + $file.Extension
            $newFullName = Join-Path -Path $file.DirectoryName -ChildPath $newName
            $counter++
        }

        # Rename the image
        Rename-Item -Path $file.FullName -NewName $newFullName
    } else {
        Write-Host "No DateTaken metadata found for $($file.FullName), skipping."
    }
}

Write-Host "All images have been renamed."
