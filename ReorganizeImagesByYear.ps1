# Prompt the user to enter the path to the directory containing the images
$sourceDir = Read-Host "Enter the path to the directory containing your images"

# Verify that the directory exists
if (-not (Test-Path -Path $sourceDir)) {
    Write-Host "The specified directory does not exist. Please try again." -ForegroundColor Red
    exit
}

# Get all image files in the source directory (including subdirectories)
$imageFiles = Get-ChildItem -Path $sourceDir -Include *.jpg, *.jpeg, *.png, *.gif, *.bmp -File -Recurse

foreach ($file in $imageFiles) {
    # Get the creation year of the image
    $lastWriteYear = (Get-ItemProperty $file.FullName).LastWriteTime.Year

    # Define the target directory based on the creation year
    $targetDir = Join-Path -Path $sourceDir -ChildPath $lastWriteYear

    # Create the target directory if it doesn't exist
    if (-not (Test-Path -Path $targetDir)) {
        New-Item -Path $targetDir -ItemType Directory
    }

    # Move the image to the target directory
    $targetPath = Join-Path -Path $targetDir -ChildPath $file.Name
    Move-Item -Path $file.FullName -Destination $targetPath
}

Write-Host "Images have been reorganized into folders by year."
