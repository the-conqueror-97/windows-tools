# Prompt the user to enter the path to the directory containing the videos
$sourceDir = Read-Host "Enter the path to the directory containing your videos"

# Verify that the directory exists
if (-not (Test-Path -Path $sourceDir)) {
    Write-Host "The specified directory does not exist. Please try again." -ForegroundColor Red
    exit
}

# Get all video files in the source directory (including subdirectories)
$videoFiles = Get-ChildItem -Path $sourceDir -Include *.mp4, *.3gp, *.mov, *.avi, *.mkv, *.flv -File -Recurse

foreach ($file in $videoFiles) {
    # Generate a SHA-256 hash for the file content
    $hash = Get-FileHash -Path $file.FullName -Algorithm SHA256 | Select-Object -ExpandProperty Hash

    # Format the new name based on the hash
    $newName = $hash + $file.Extension

    # Define the full path for the new name
    $newFullName = Join-Path -Path $file.DirectoryName -ChildPath $newName

    # If a file with the new name already exists, append a counter
    $counter = 1
    while (Test-Path -Path $newFullName) {
        $newName = $hash + "_$counter" + $file.Extension
        $newFullName = Join-Path -Path $file.DirectoryName -ChildPath $newName
        $counter++
    }

    # Rename the video
    Rename-Item -Path $file.FullName -NewName $newFullName
}

Write-Host "All videos have been renamed."
