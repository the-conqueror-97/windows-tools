# Prompt the user to enter the path to the directory containing the images
$sourceDir = Read-Host "Enter the path to the directory containing your images"

# Verify that the directory exists
if (-not (Test-Path -Path $sourceDir)) {
    Write-Host "The specified directory does not exist. Please try again." -ForegroundColor Red
    exit
}

# Get all image files in the source directory (including subdirectories)
$imageFiles = Get-ChildItem -Path $sourceDir -Include *.jpg, *.JPG, *.jpeg, *.png, *.gif, *.bmp, *.HEIC -File -Recurse

# Create a hashtable to store the hash values of the images
$hashTable = @{}

# Define a function to calculate the hash of a file
function Get-FileHash($filePath) {
    $hasher = [System.Security.Cryptography.HashAlgorithm]::Create('MD5')
    $stream = [System.IO.File]::OpenRead($filePath)
    $hashBytes = $hasher.ComputeHash($stream)
    $stream.Close()
    return [BitConverter]::ToString($hashBytes) -replace '-'
}

foreach ($file in $imageFiles) {
    # Calculate the hash of the current image
    $fileHash = Get-FileHash $file.FullName

    # Check if the hash already exists in the hashtable
    if ($hashTable.ContainsKey($fileHash)) {
        # If a duplicate is found, delete the current image
        Write-Host "Deleting duplicate image: $($file.FullName)"
        Remove-Item -Path $file.FullName
    } else {
        # If no duplicate is found, add the hash to the hashtable
        $hashTable[$fileHash] = $file.FullName
    }
}

Write-Host "Duplicate images have been removed."
