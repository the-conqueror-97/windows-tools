# Prompt the user to enter the path to the directory containing the videos
$sourceDir = Read-Host "Enter the path to the directory containing your videos"

# Verify that the directory exists
if (-not (Test-Path -Path $sourceDir)) {
    Write-Host "The specified directory does not exist. Please try again." -ForegroundColor Red
    exit
}

# Get all video files in the source directory (including subdirectories)
$videoFiles = Get-ChildItem -Path $sourceDir -Include *.mp4, *.MP4, *.mov, *.MOV, *.avi, *.AVI -File -Recurse

# Create a hashtable to store the hash values of the videos
$hashTable = @{}

# Define a function to calculate the hash of a file
function Get-FileHash($filePath) {
    $hasher = [System.Security.Cryptography.HashAlgorithm]::Create('MD5')
    $stream = [System.IO.File]::OpenRead($filePath)
    $hashBytes = $hasher.ComputeHash($stream)
    $stream.Close()
    return [BitConverter]::ToString($hashBytes) -replace '-'
}

foreach ($file in $videoFiles) {
    # Calculate the hash of the current video
    $fileHash = Get-FileHash $file.FullName

    # Check if the hash already exists in the hashtable
    if ($hashTable.ContainsKey($fileHash)) {
        # If a duplicate is found, rename the current video by adding 'duplicate_' as a prefix
        $duplicateName = "duplicate_" + $file.Name
        $duplicatePath = Join-Path -Path $file.DirectoryName -ChildPath $duplicateName
        
        # Ensure the new name does not conflict with existing files
        $counter = 1
        while (Test-Path -Path $duplicatePath) {
            $duplicateName = "duplicate_" + $counter + "_" + $file.Name
            $duplicatePath = Join-Path -Path $file.DirectoryName -ChildPath $duplicateName
            $counter++
        }
        
        Write-Host "Renaming duplicate video: $($file.FullName) to $duplicatePath"
        Rename-Item -Path $file.FullName -NewName $duplicatePath
    } else {
        # If no duplicate is found, add the hash to the hashtable
        $hashTable[$fileHash] = $file.FullName
    }
}

Write-Host "Duplicate videos have been renamed with 'duplicate_' prefix."
