# Prompt the user to enter the path to the directory containing the images
$sourceDir = Read-Host "Enter the path to the directory containing your images and videos"

# Verify that the directory exists
if (-not (Test-Path -Path $sourceDir)) {
    Write-Host "The specified directory does not exist. Please try again." -ForegroundColor Red
    exit
}

# Get all image and video files in the source directory (including subdirectories)
$imageFiles = Get-ChildItem -Path $sourceDir -Include *.jpg, *.jpeg, *.png, *.gif, *.bmp, *.mp4, *.mov, *.avi -File -Recurse

foreach ($file in $imageFiles) {
    # Get the last write time of the current file
    $lastWriteTime = (Get-ItemProperty $file.FullName).LastWriteTime

    # Format the new name based on the last write time
    $newNameBase = $lastWriteTime.ToString("yyyy-MM-dd_HH-mm-ss")
    $newName = $newNameBase + $file.Extension

    # Define the full path for the new name
    $newFullName = Join-Path -Path $file.DirectoryName -ChildPath $newName

    # Initialize a counter to handle naming conflicts
    $counter = 1

    # If a file with the new name already exists, modify the name to include a counter
    while (Test-Path -Path $newFullName) {
        $newName = $newNameBase + "_$counter" + $file.Extension
        $newFullName = Join-Path -Path $file.DirectoryName -ChildPath $newName
        $counter++
    }

    # Rename the file
    Rename-Item -Path $file.FullName -NewName $newFullName
}

Write-Host "All images and videos have been renamed."